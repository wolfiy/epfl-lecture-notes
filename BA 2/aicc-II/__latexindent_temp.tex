\documentclass[12pt, a4paper]{report}

\usepackage{lmodern}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[pdfauthor = {{wolfiy}}, pdftitle = {{AICC II}}, pdfstartview = Fit, pdfpagelayout = SinglePage , pdfnewwindow = true, bookmarksnumbered = true, breaklinks, colorlinks, linkcolor = black, urlcolor = blue, citecolor = gray, linktoc = all]{hyperref}
\usepackage{multicol}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{parskip}
\usepackage{subcaption}

\theoremstyle{plain}
\newtheorem*{theorem}{Theorem}
\newtheorem*{lemma}{Lemma}
\newtheorem*{proposition}{Proposition}
\newtheorem*{corollary}{Corollary}

\theoremstyle{definition}
\newtheorem*{definition}{Definition}

\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\newtheorem*{observation}{Observation}
\newtheorem*{propriety}{Propriety}
\newtheorem*{example}{Example}

\setcounter{chapter}{-1}
\newcommand{\RE}{\mathrm{Re}}
\newcommand{\IM}{\mathrm{Im}}

\date{\today}
\title{\textbf{AICC II}}
\author{wolfiy.\#0405 \\ IN BA2 (Michael \textsc{C. Gastpar})}
\date{\today}

\begin{document}
\maketitle

\begin{abstract}
    This document will contain theorems, definitions, rules and tips useful to the AICC II class.

    I gathered information from M. Michael \textsc{C. Gastpar}'s course.

    If you have any questions or remarks, you can reach me on Discord at "wolfiy.\#0405". The latest verison of this file can be found \href{https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%202/aicc-II/aicc-II.pdf}{here}.
\end{abstract}

\tableofcontents

\chapter{Review of probability}

\begin{definition}[sample space]
    Set of all possible outcomes. $\Omega = \{\omega_1, \dots, \omega_n\}$.
\end{definition}

\begin{definition}[event]
    Subset of $\Omega$. $p(E) = \dfrac{|E|}{|\Omega|}$, if equally likely.
\end{definition}

\begin{definition}[conditional probability]
    Probability that $E$ occurs, given that $F$ has occured. $p(E|F) = \dfrac{|E \cap F|}{|F|} = \dfrac{p(E \cap F)}{p(F)}$.
\end{definition}

\begin{definition}[independent events]
    If $p(E|F) = p(E)$, $p(E \cap F) = p(E)p(F)$.
\end{definition}

\begin{definition}[disjoint events]
    If $p(E_1 \cup E_2) = p(E_1) + p(E_2)$.
\end{definition}

\begin{definition}[probability mass function]
    A function $p: \Omega \to [0,1]$ such that $\displaystyle\sum_{\omega \in \Omega}p(\omega) = 1$. We also have $p(E) = \displaystyle\sum_{\omega \in E}p(\omega)$.
\end{definition}

\begin{theorem}[Law of total probability]
    $p(E) = p(E|F)p(F) + p(E|F^C)p(F^C)$.

    More generally, $p(E) = p(E|F_1)p(F_1) + p(E|F_2)p(F_2) + \dots + p(E|F_n)p(F_n)$.
\end{theorem}

\begin{theorem}[Bayes' rule]
    $p(F|E) = \dfrac{p(E|F)p(F)}{p(E)}$
\end{theorem}

\begin{definition}[random variable]
    Function $X: \Omega \to \mathbb{R}$.
\end{definition}

\begin{definition}[probability distribution]
    $p_X(x)$ is the probability that $X=x$, i.e. the probability of $E = \{\omega \in \Omega : X(\omega) = x\}$.
\end{definition}

\begin{definition}[marginal dist.]
    $p_X(x) = \displaystyle\sum_{y}p_{X,Y}(x,y)$, with respect to $x$.
\end{definition}

\begin{definition}[expected value]
    $\mathbb{E}[X] = \displaystyle\sum_{\omega}X(\omega)p(\omega) = \displaystyle\sum_{x}xp_X(x)$.
\end{definition}

\begin{remark}
    Expectation is a linear operation: $\mathbb{E}\left[\displaystyle\sum_{i=1}^{n}X_i\alpha_i\right] = \displaystyle\sum_{i=i}^{n}\alpha_i\mathbb{E}[X_i]$.
\end{remark}

\begin{definition}[indep. random var.]
    $p_{X,Y}(x,y) = p_X(x)p_Y(y)$.

    We have equivalences between:
    \begin{multicols}{2}
        $p_{X,Y} = p_xp_y$;

        $p_{Y|X}(y|x) = p_Y(y)$;

        $p_{Y|X}(y|x)$ not a function of $x$;

        $p_{X|Y}(x|y) = p_X(x)$;

        $p_{X|Y}(x|y)$ not a function of $y$.
    \end{multicols}
\end{definition}

\begin{remark}
    If $X,Y$ are independant, then $\mathbb{E}[XY] = \mathbb{E}[X]\mathbb{E}[Y]$.
\end{remark}

\begin{definition}[conditional distribution of $Y$ given $X$]
    $p_{Y|X}(y|x) = \dfrac{p_{X,Y}(x,y)}{p_X(x)}$.
\end{definition}

\chapter{Sources and entropy}
In the digital world, we communicate by revealing the value of a sequence of variables called information symbols. The $i^{th}$ symbbol might be the intensity of a pixel, the score in an exam, a bit,
a letter, etc.

A symbol provides information only if there had been other possibilities for its values. Mathematically, this means that a symbol provides information only if it is a non-constant random variable.

How much information is carries by a symbol $S$? $\log|A|^n = n \log|A|$.

\section{Entropy}
Notion of uncertainty associated to a discrete random variable.
$$H_b(S) = -\displaystyle\sum_{s \in \text{supp}(p_S)}p_S(s)\log_bp_S(s) = -\displaystyle\sum_{s \in \mathcal{A}}p_S(s)\log_b{p_S(s)} = \mathbb{E}[-\log{p_S(s)}]$$
where $\text{supp}(p_S) = \{s:p_S(s) > 0\}$.

\begin{remark}
    $s \in \text{supp}(p)$ is nedded because the logarithm is not defined for $0$. To simplify things, we also declare that $p_S(s)\log{p_S(s)} = 0$ when $p_S(s) = 0$.
\end{remark}

\begin{propriety}
    The entropy is equal to Hartley's measure of information iff the random variable has uniform distribution.
\end{propriety}

\subsection{Binary entropy function}
When $|\mathcal{A}| = 2$. Then $p_S$ has only two possible values: $p, (1-p)$. Thus:
$$H(S) = h(p) = -p\log_2{p} - (1-p)\log_2{(1-p)}$$

\subsection{Entropy bounds}
\begin{lemma}
    For $r \in \mathbb{R}_+$ and with equality iff $r=1$, $\log_b{r} \leq (r-1)\log_b(e)$.
\end{lemma}

\begin{theorem}
    The entropy of a discrete random variable $S \in \mathcal{A}$ satisfies
    $$0 \leq H_b(S) \leq \log_b{|A|}$$
    with equality on the left iff $p_S(s) = 1$ for some $s$, and equality on the right iff $p_S(s) = \frac{1}{|\mathcal{A}|}\  \forall s$.
\end{theorem}

\subsection{Multiple random variables}
$X, Y$ with joint probability distribution $p_{X,Y}$. Then $$H(X,Y) = \mathbb{E}[-\log{p_{X,Y}(X,Y)}] = 
-\displaystyle\sum_{(x,y)\in\mathcal{X}\times\mathcal{Y}}p_{X,Y}(x,y)\log{p_{X,Y}(x,y)}$$

\begin{theorem}
    Let $S_1, \dots, S_n$ be discrete random variables. Then
    $$H(S_1, S_2, \dots, S_n) \leq H(S_1) + H(S_2) + \dots + H(S_n)$$
    with equality iff $S_1, \dots, S_n$ are independents.
\end{theorem}

\chapter{Source coding}
Source coding is often seen as a way to compress the source. More generally, the goal of source coding is to efficiently describe the source
output. For a fixed description alphabet (often binary), we want to minimze the average description length.

\begin{center}
    \includegraphics[width=0.8\linewidth]{assets/02001.png}
\end{center}

The encoder is specified by
\begin{itemize}
    \item The input alphabet $\mathcal{A}$ (same as source);
    \item The output alphabet $\mathcal{D}$ (typically 0,1);
    \item The codebook $\mathcal{C}$ (finite sequences over $\mathcal{D}$);
    \item The one-to-one encoding map $\Gamma: \mathcal{A}^k \to \mathcal{C}$, where $k \in \mathbb{Z}$.
\end{itemize}
For now, $k = 1$.

\begin{definition}[uniquely decodable]
     If every concatenation of codewords has a unique parsing into a sequence of codewords.

     To make a code uniquely decodable, we can use a separator at the end or beginning of each codeword, or use a fixed-length code.
\end{definition}

\begin{definition}[prefix-free]
    A code in which no codeword is a prefix of another codeword. Also called instantaneous code.
\end{definition}

\begin{remark}
    A prefix-free code is always uniquely decodable. However a uniquely decodable code is not necessarily prefix-free.
\end{remark}

\section{Trees}
\subsection{Complete trees}
\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.2\linewidth}
        \includegraphics[width=1\linewidth]{assets/02002.png}
    \end{subfigure}
    \begin{subfigure}{0.37\linewidth}
        \includegraphics[width=1\linewidth]{assets/02003.png}
    \end{subfigure}
    \begin{subfigure}{0.37\linewidth}
        \includegraphics[width=1\linewidth]{assets/02004.png}
    \end{subfigure}
\end{figure}

\subsection{Decoding trees}
Obtained from the complete tree by keeping only branches that form a codeword. Useful to visualize the decoding process.

\section{Kraft-McMillan}
\begin{theorem}
    If a $D$-ary code is uniquely decodable then its codeword lengths $l_1, \dots, l_M$ satisfy Kraft's inequality\footnote{Contrapositive: 
    If a $D$-ary code's codeword lengths do not satisfy the inequality, the the code is not uniquely decodable}:
    $$D^{-l_1} + \dots + D^{-l_M} \leq 1$$
\end{theorem}

\begin{theorem}
    If the positive integers $l_1, \dots, l_M$ satisfy Kraft's inequality for some positive integer $D$, then there exsists a $D$-ary prefix-free
    code (hence uniquely decodable) that has codeword lengths $l_1, \dots, l_m$.
\end{theorem}

\begin{definition}[average codeword length]
    Let $l(\Gamma(s))$ be the length of the codeword associated to $s \in \mathcal{A}$. The average codeword-length is
    $$L(S,\Gamma) = \sum_{s \in \mathcal{A}} p_S(s)l(\Gamma(s))$$
\end{definition}

\begin{remark}
    The units of $L(S, \Gamma)$ are "code symbols". When $D = 2$, the units are bits.
\end{remark}

\begin{theorem}
    Let $\Gamma: \mathcal{A} \to \mathcal{C}$ be the encoding map of the $D$-ary code for the random variable $S \in \mathcal{A}$.

    If the code is uniquely decodeable, then the average codeword-length is lower bounded by the entropy of $S$, namely
    $$H_D(S) \leq L(S, \Gamma)$$
    with equality iff, $\forall s \in \mathcal{A},\ p_S(s) = D^{-l(\Gamma(s))}$. An equivalent condition is $l(\Gamma(s)) = \log_D{\frac{1}{p_S(s)}}$.
\end{theorem}

\begin{theorem}
    For every random variable $S \in \mathcal{A}$ and every integers $D \geq 2$, there exists a prefix-free $D$-ary code for $S$ such that, $\forall s \in \mathcal{A}$
    $$l(\Gamma(s)) = \lceil-\log_D{p_S(s)}\rceil$$
    Such codes are called $D$-ary Shannon-Fano codes.
\end{theorem}

\begin{theorem}
    The average codeword-length of a $D$-ary Shannon-Fano code for the random variable $S$ fulfills
    $$H_D(S) \leq L(S, \Gamma_{SF}) < H_D(S) + 1$$
\end{theorem}

\begin{corollary}
    The Huffman code also satisfies these bounds.
\end{corollary}

\section{Optimal code}
\subsection{Huffman code}
Unlike the Shannon-Fano code, the construction of the Huffman code starts from the leaves, and merge the two least likely. Such code for a random variable is prefix-free and optimal, in the sense that no
code can achieve a smaller average codeword-length.

\begin{center}
    \includegraphics[width=0.5\linewidth]{assets/02005.png}
\end{center}

\begin{lemma}
    The average path length of a tree with probabilities is the sum of the probabilities of the intermediate nodes (root included):
    $$\sum_ip_il_i = \sum_jq_j$$
\end{lemma}

\begin{theorem}[optimal construction]
    If $\Gamma_H$ is a Huffman code (prefix-free by construction) and $\Gamma$ is another uniquely decodeable code for the same source $S$, then it is guarenteed that
    $$L(S,\Gamma_H) \leq L(S,\Gamma)$$
\end{theorem}

\subsubsection{Facts}
\begin{enumerate}
    \item In the decoding tree of an optimal binary code, each intermediate node has exactly two offsprings. In particular, any leaf at depth $S$ has siblings.
    \item An optimal encoder assigns shorter codewords to higher-probability letters. This and fact 1 imply that two of the least likely codewords have length $L$.
    \item Based on fact 2, without loss of optimality, we may require that two of the least-likely leaves be siblings at depth $L$.
\end{enumerate}

\begin{observation}
    A ternary tree "must" have an odd number of leaves.
\end{observation}

With Huffman, every step reduces alphabet by $D-1$ symbols. At the end, we want exactly $D$ symbols. We need $D + k(D-1),\ k \in \mathbb{Z}_+$ to begin with.

\chapter{Conditional entropy}

\section{Compressing long strings}
\begin{theorem}
    The average codeword-length of a $D$-ary Shannon-Fano code for the random variable $(S_1, S_2)$ fulfills
    $$H_D(S_1, S_2) \leq L((S_1, S_2), \Gamma_{SF}) < H_D(S_1, S_2) + 1$$
\end{theorem}

\begin{theorem}
    The per-letter average codeword-length of a $D$-ary Shannon-Fano code for the random variable $(S_1, S_2)$ fulfills
    $$\frac{H_D(S_1, S_2)}{2} \leq \dfrac{L((S_1, S_2), \Gamma_{SF})}{2} < \frac{H_D(S_1, S_2)}{2}$$
    Equivalently, for $n$ symbols
    $$H_D(S) \leq \frac{L((S_1, S_2, \dots, S_n), \Gamma_{SF})}{n} < H_D(S) + \frac{1}{n}$$
\end{theorem}

\begin{remark}
    This holds in generality; it is not assumed that $S_i$ are independant and identically distributed.
\end{remark}

\begin{definition}[conditional probability of $X$ given $Y=y$]
    $$H_D(X | Y = y) = -\displaystyle\sum_{x \in \mathcal{X}}p_{X|Y}(x|y)\log_D{p_{X|Y}}(x|y)$$
\end{definition}

\begin{theorem}
    The conditional entropy of a discrete random variable $X \in \mathcal{X}$ conditioned on $Y = y$ satisfies
    $$0 \leq H_D(X | Y = y) \leq \log_D{\mathcal{X}}$$
    with equality on the left iff $p_{X|Y}(x|y) = 1$ for some $x$, and equality on the right iff $p_{X|Y}(x|y) = \frac{1}{|\mathcal{X}|}$ for all $x$.
\end{theorem}

\begin{definition}[conditional entropy of $X$ given $Y$]
    Is given by:
    $$H_D(X | Y) = \sum_{y \in \mathcal{Y}}p_Y(y)\left(-\sum_{x \in \mathcal{X}}p_{X|Y}(x|y)\log_D{p_{X|Y}(x|y)}\right)$$    
\end{definition}

\begin{theorem}
    The conditional entropy of a discrete random variable $X \in \mathcal{X}$ conditioned on $Y$ satisfies
    $$0 \leq H_D(X | Y) \leq \log_D{|\mathcal{X}|}$$
    with equality on the left iff for every $y$ there exists $x:p_{X|Y}(x|y) = 1$, and equality on the right iff $p_{X|Y}(x|y) = \frac{1}{|\mathcal{X}|}$.
\end{theorem}

\begin{theorem}[conditioning reduces entropy]
    For any two discrete random variables $X$ and $Y$,
    $$H_D(X|Y) \leq H_D(X)$$
    with equality iff $X,Y$ are independant random variables.
\end{theorem}

\begin{remark}
    $H(S_4 | S_3, S_2, S_1) = H(S_4 | S_1, S_2, S_3)$ 
\end{remark}

\begin{theorem}
    Let $S_1, \dots, S_n$ be discrete random variables. Then
    $$H_D(S_1, S_2, \dots, S_n) = H_D(S_1) + H_D(S_2|S_1) + \dots + H_D(S_N | S_1, \dots, S_{n-1})$$
\end{theorem}

\begin{theorem}
    Let $S_1, \dots, S_n$ be discrete random varibales. Then
    $$H(S_1, \dots, S_n) \leq H(S_1) + \dots + H(S_n)$$
    with equality iff $S_1, \dots, S_n$ are independent.
\end{theorem}

\begin{remark}
    Sometimes, it is convenient to compute the conditional entropy using the chain rules, like here: $H(X|Y) = H(X,Y) - H(Y)$.
\end{remark}

\begin{corollary}
    We have that
    $$H(X,Y) \geq H(X)$$
    $$H(X,Y) \geq H(Y)$$
\end{corollary}

\chapter{Random processes}
\begin{definition}
    The source $\mathcal{S} = (S_1, S_2, \dots)$ is said to be regular if
    $$H(\mathcal{S}) = \lim\limits_{n \to \infty} H(S_n)$$
    $$H^*(\mathcal{S}) = \lim\limits_{n \to \infty}H(S_n | S_1, S_2, \dots, S_{n-1})$$
    exist and are finite.

    For a regular source $\mathcal{S}$, $H(\mathcal{S})$ is called the entropy of a symbol, and the other the entropy rate.
\end{definition}

\begin{definition}[coin-flip source]
    The source models a sequence $S_1, S_2, \dots, S_n$ of $n$ coin flips.

    So $S_i \in \mathcal{S} = \left\{H,T\right\}$, where $H$ stands for heads, and $T$ for tails, $i = 1, 2, \dots, n$.

    $p_{s_i}(H) = p_{s_i}(T) = \frac{1}{2},\ $
\end{definition}

\end{document}