# Généricité

Certains types peuvent être inconnus lors de l'écruiture d'une classe (p.ex. dans les classes "conteneurs" qui stockent d'autres objets de type quelconque).

## Cellule

Objet immuable dont le seul but est de contenir un unique autre objet.

### Cellule de chaine

Cellule restreint à stocker une chaine de caractères.

```java
public final class CellOfString {
  private final String s;

  public CellOfString(String s) { this.s = s; }
  public String get() { return s; }
}

// Utilisation
CellOfString message = new CellOfString("Bonne année 2021");
System.out.println(message.get());
```

### Cellule d'objet

Capable de stocker un objet de type `Object`.

```java
public final class Cell {
  private final Object o;

  public Cell(Object o) { this.o = o; }
  public Object get() { return o; }
}
```

## Généricité

Aussi appelé polymorphsime paramétrique.

### Cellule générique

```java
public final class Cell<E> {
  private final E e;

  public Cell(E e) { this.e = e; }
  public E get() { return e; }
}
```

Cette classe est générique, et `E` est sont paramètre de type. Pour utiliser la cellule, il faut spécifier le type:

```java
Cell<String>        // lire: cell of string
Cell<Date>          // lire: cell of date
Cell<Cell<String>>  // lire: cell of cell of string
```

Cela permet d'éviter le transtypage (contrairement aux cellules d'objet).

## Paire générique

Permet de représenter une paire de valeurs, i.e. une cellule à deux éléments.

```java
public final class Pair<F, S> {
  private final F fst;
  private final S snd;

  public Pair(F fst, S snd) {
    this.fst = fst;
    this.snd = snd;
  }
  public F fst() { return fst; }
  public S snd() { return snd; }
}
```

### Méthode générique

```java
public final class Cell<E> {
  private final E e;
  // … comme avant
  public <S> Pair<E, S> pairWith(S s) {
    return new Pair<>(e, s);
  }
}

// Appel
Cell<String> message = new Cell<>("Bonne année ");
Pair<String, Date> pair = message.pairWith(Date.today());
System.out.println(pair.fst() + pair.snd().year());
```

### Types primitifs

_Remarque:_ ils ne peuvent pas être utilisés comme paramètre d'un type générique. Il faut alors les emballer.

```java
Cell<Integer> c = new Cell<>(Integer.valueOf(1));
int succ = c.get().intValue() + 1; // Déballage avant + 1.
```

L'emballage et le déballage est automatiquement généré par Java:

```java
Cell<Integer> c = new Cell<>(1);
int succ = c.get() + 1;
```

### Bornes

```java
public final class Cell<E extends Number> { // besoin du extends sinon
  private final E e;                        // appel invalide (E pourrait
  // … comme avant                          // être un String p.ex.)
  public double getInverse() {
    return 1.0 / e.doubleValue();
  }
}
```

### Types bruts

Introduits pour éviter les problèmes de compatibilité entre la généricité et ce qui a été inventé avant. L'interaction est:

- une version générique peut être utilisée partout où la version brute est attendue, sans provoquer l'affichage d'un avertissement;
- la version brute peut être utilisée partout où une version générique est attendue, mais cela provoque l'affichage d'un avertissement.

---

**Notes**

Ne _jamais_ utiliser de types brutes; ils n'existent que pour faciliter la migration d'ancien code.

---

## Limitations

1. La création de tableaux dont les éléments ont un type générique est interdite;
2. Les tests d'instance impliquant des types génériques sont interdits;
3. Les transtypages (*casts*) sur des types génériques ne sont pas surs, c-à-d qu'ils produisent un avertissement lors de la compilation  et un résultat éventuellement incorrect à l'exécution;
4. La définition d'exceptions génériques est interdite.
