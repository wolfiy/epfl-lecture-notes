# Patrons de conception

## 1 Introduction

Solution à un problème de conception récurant.

*Exemple:* l'itérateur est un patron de conception du problème:

> Un objet possède une collection de valeurs et désire y donner accès, sans révéler la manière dont cette collection est représentée en interne.

résolu ainsi :

> Fournir un itérateur, à savoir un objet qui permet d'examiner les valeurs les unes après les autres, dans un ordre donné.

### 1.1 Avantages et inconvénients des patrons

- Permettent de diffuser largement les meilleures solutions;

- Permet de communiquer à un niveau plus haut d'abstraction.

Mais:

- Peut augmenter la complexité du programme.

### 1.2 Patrons et langages de programmation

Un patron ne peut pas être écrit une fois pour toutes et inclus dans une bibliothèque. Au lieu de cela, c'est à la personne écrivant un programme que revient la responsabilité d'appliquer le patron en écrivant les classes correspondantes.

*Remarque:* un patron de conception n'est normalement pas lié à un langage de programmation donné.

## 2 Description des patrons

Un patron est composé de:

- son nom;
- une description du problème résolu;
- une description de la solution à ce problème;
- une présentation des conséquences liées à l'utilisation du patron.

### 2.1 Diagramme de classes

*Exemple:* Monopoly.

<img title="" src="file:///Users/wolfiy/Documents/Code/latex/EPFL%20Lecture%20Notes/BA%202/ppoo/assets/06001.png" alt="" data-align="center" width="417">

Pour représenter des classes fictives, on utilise des pointillés; c'est pour montrer une possible organisation.

<img title="" src="file:///Users/wolfiy/Documents/Code/latex/EPFL%20Lecture%20Notes/BA%202/ppoo/assets/06002.png" alt="" data-align="center" width="289">

## 3 Le patron *Builder*

Sur un exemple, avec un `Builder`.

### 3.1 Illustration du problème

On veut une bibliothèque de calcul matriciel. Toutes les classes étant des entités mathématiques, elles doivent être immuables, ce qui implique que tous les éléments de la matrice doivent être spécifié à la construction.

### 3.2 Solution

Une solution peut être d'ajouter un bâtisseur qui lui est modifiable et possède les méthodes pour changer les éléments de la matrice en cours de création.

```java
public interface Matrix {
 double get(int r, int c);
 Matrix transpose();
 Matrix inverse();
 Matrix add(Matrix that);
 Matrix multiply(double that);
 Matrix multiply(Matrix that);
 // … autres méthodes
}
```

Ainsi, on pourrait avoir:

```java
public final class DenseMatrix implements Matrix { … }
public final class SparseMatrix implements Matrix { … }
```

```java
public final class MatrixBuilder {
  private double[][] elements;

  public MatrixBuilder(int r, int c) {
    elements = new double[r][c];
  }

  public double get(int r, int c) {
    return elements[r][c];
  }

  public void set(int r, int c, double v) {
    elements[r][c] = v;
  }

  public Matrix build() {
    if (density() > 0.5)
      return new DenseMatrix(elements);
    else
      return new SparseMatrix(elements);
  }
}
```

<img title="" src="file:///Users/wolfiy/Documents/Code/latex/EPFL%20Lecture%20Notes/BA%202/ppoo/assets/06003.png" alt="" width="317" data-align="center">

### 3.3 Généralisation

Chaque fois que le processus de construction d'une classe est assez difficile pour que l'on désire le découper en plusieurs étapes, on peut utiliser un objet pour stocker l'état de l'objet en cours de construction; c'est l'idée du patron de conception *Builder*.

<img title="" src="file:///Users/wolfiy/Documents/Code/latex/EPFL%20Lecture%20Notes/BA%202/ppoo/assets/06004.png" alt="" width="310" data-align="center">

## 4 Le patron *Decorator*

### 4.1 Illustration du problème

Admettons que l'on désire écrire un programme de dessin vectoriel 2D, permettant de dessiner et manipuler différentes formes géométriques. Celles-ci sont représentées par une interface `Shape` et des mises en œuvre propres. 

```java
public interface Shape {
 public boolean contains(Point p);
 // … autres méthodes
}
public final class Circle implements Shape {
 …
}
// … Polygon, etc.
```

On désire offrir la possibilité d'appliquer différentes transformations géométriques aux formes de base : translation, rotation, symétrie, etc. Comment faire ?

Une solution consiste à définir des pseudo-formes qui en transforment d'autres. Par exemple pour la translation :

```java
public final class Translated implements Shape {
 private final Shape shape;
 private final double dx, dy;
 public Translated(…) { … }
 public boolean contains(Point p) {
 return s.contains(new Point(p.x() - dx, p.y() - dy));
 }
 // … autres méthodes
}
```

Le diagramme de classes ci-dessous montre les classes impliquées dans cet exemple.

<img title="" src="file:///Users/wolfiy/Documents/Code/latex/EPFL%20Lecture%20Notes/BA%202/ppoo/assets/06005.png" alt="" width="262" data-align="center">

### 4.2 Généralisation

Chaque fois que l'on désire changer le comportement d'un objet sans changer son interface, on peut « l'emballer » dans un objet ayant la même interface mais un comportement différent. L'objet « emballant » laisse l'objet emballé faire le gros du travail, mais modifie son comportement lorsque cela est nécessaire.

Cette solution est décrite par le patron *Decorator*, aussi souvent appelé *Wrapper*.

<img title="" src="file:///Users/wolfiy/Documents/Code/latex/EPFL%20Lecture%20Notes/BA%202/ppoo/assets/06006.png" alt="" width="310" data-align="center">
