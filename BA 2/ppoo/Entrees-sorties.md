# Entrées sorties

Echange de donnée entre un programme et le monde extérieur. Dans la plupart des cas via:

- l'utilisateur;

- un ou plusieurs fichiers;

- une ou plusieures connexions réseau.

## Fichiers et systèmes de fichiers

_Fichier:_ séquence d'octets stockés sur un suport physique.

_Méta-données:_ informations sur le fichier.

_Système de fichier:_ organise un ensemble de fichiers dans des répertoires.

_Fichiers textuels:_ contiennent une séquence de caractères.

_Fichiers binaires:_ le reste.

## I/O en Java

Paquetages: `java.io` et `java.nio` (plus récent mais ne remplace pas).

L'abstraction des entrées/sorties en Java est le flot: une séquence de valeurs auxquelles ont accède de manière séquentielle.

### Flots d'entrée

Deux sous-classe d'`InputStream`:

- Flots d'entrée primaires (obtenir les données directement);

- Flots d'entrée filtrants (obtenir les données d'autres flots et les transformer à la volée).

```java
// Programme bloqué jusqu'à l'arrivée des octets.
int read()    // -1 à la fin du flot
int readNBytes(byte[] b, int o, int l)
byte[] readNBytes(int l)
byte[] readAllBytes()
long skip(long n)
void skipNBytes(long n)
// Pour éviter de bloquer
int available()

long transferTo(OutputStream o)
boolean markSupported()
void mark(int l) // Position pour reset
void reset()

void close() // rend le flot inutilisable!
```

### Flots d'entrée concrets

```java
InputStream s = new FileInputStream("file.bin"); // d'un fichier

byte[] b = new byte[]{ 1, 2, 3 };
InputStream s = new ByteArrayInputStream(b); // d'un tableau

InputStream s =  new BufferedInputStream(new FileInputStream(…));

GZIPInputStream... // décompresse les zip
```

### Flots de sortie

Même catégories que les entrées.

```java
void write(int b) // 8 LSB (24 MSB ignorés)
void write(byte[] b, int o, int l)
void write(byte[] b)
void flush() // force à être effectivement écrites

void close()
```

### Flots de sortie concrets

Pareil que pour les entrée, mais avec `OutputStream`.

### Resources

Il est important de fermer les flots lorsqu'ils sont devenus inutiles! Examples de fermeture:

```java
try (InputStream s = new FileInputStream(…)) { // close appelé automat.
  System.out.println(s.available());
}


try (InputStream i = new FileInputStream("in.bin");
     OutputStream o = new FileOutputStream("out.bin")) {
  // … code utilisant les flots i et o
} catch (IOException e) {
  // … code gérant l'exception
} finally {
  System.out.println("done!");
}

// NOTE: il faut implémenter AutoClosable
public interface AutoCloseable {
  void close();
}
```

## Représentation des caractères

Les caractères doivent être représenté sous forme binaire.

### Unicode

Standard ayant comme but d'être universel, donc de permettre la représentation de tout les caractères existants: math, émojis, alphabets non latin, etc.

- UTF-8 et UTF-16 sont à longueur variable;

- UTF-32 à longueur fixe.

#### UTF-8

Le caractère y est représenté par une séquence de 1 à 4 octets, selon la plage à laquelle il appartient. En base 16:

- 1 octet : de 0 à 7F;
- 2 octets : de 80 à 7FF;
- 3 octets : de 800 à FFFF;
- 4 octets : de 10000 à 10FFFF.

_Remarque:_ plus compact que les deux autres.

#### UTF-16

Même principe:

- 2 octets : de 0 à D7FF et de E000 à FFFF;
- 4 octets : de 10000 à 10FFFF.

La quasi totalité des alphabet en usage est encodé par un unique mot de 16 bits. Les émoticônes par contre appartiennent à la seconde plage.

---

**Note**

Les premiers caractères d'Unicode sont les même que ceux d'ASCII.

---

## Caractères en Java

Lors de la conception de Java, Unicode ne représentait les caractères que par 16 bits, d'où le fait que `char` soit 16 bits. Pour écrire les caractères "plus loin":

```java
String s = "œuf à €1";
String s = "œuf à \u20AC1";
String thumbsUp = "\uD83D\uDC4D"; // emoji
System.out.println(thumbsUp);
```

### Méthodes des chaines

```java
String thumbsUp = "\uD83D\uDC4D"; // emoji
thumbsUp.length();             // 2
thumbsUp.charAt(0);            // 0xD83D
thumbsUp.charAt(1);            // 0xDC4D
thumbsUp.codePointCount(0, 2); // 1
thumbsUp.codePointAt(0);       // 0x1F44D
```

## Entrées/sorties textuelles

Dans le cas général, il n'est pas possible d'identifier avec certitude l'encodage d'un fichier textuel, mais:

1. certaines séquences d'octets sont invalides dans certains encodages à
   longueur variable comme UTF–8 ou UTF–16, permettant — si on les 
   rencontre — d'éliminer ces candidats,
2. au moyen de statistiques, on peut déterminer qu'un décodage 
   produisant la chaîne « bœuf » a plus de chances d'être correct qu'un 
   décodage produisant la chaîne « bÅ“uf ».

On a aussi que:

1. le retour de chariot (*carriage return* ou CR), dont le code est 13 en ASCII, UTF–8 et autres,
2. le saut de ligne (*line feed* ou LF), dont le code est 10 en ASCII, UTF–8 et autres,
3. le retour de chariot suivi du saut de ligne (souvent abrégée CRLF).

### En java

Classes `Reader` et `Writer`.

```java
abstract public class Reader {
  int read();
  int read(char[] c, int o, int l);
  int read(char[] c);

  long transferTo(Writer w);

  long skip(long n);
  boolean ready();

  boolean markSupported();
  void mark(l);
  void reset();

  void close();
  // A NOTER QUE: il  n'y a pas de méthode available, ce qui peut
  // être couteux.
}


abstract public class Writer {
  void write(int c);
  void write(char[] a);
  void write(char[] a, int o, int l);
  void write(String s);

  Writer append(char c);
  Writer append(CharSequence c);
  Writer append(CharSequence c, int s, int e);

  void flush();

  void close();
}
```
