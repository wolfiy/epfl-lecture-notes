# Test unitaire

Pour des grands projets, il n'est pas possible de tester uniquement tout à la fin; il faut tester le programme petit à petit.

## A faire

1. Un cas normal au moins;

2. Cas limite;

3. Cas d'erreur.

## JUnit

Identifiés par l'annotation `@Test`.

---

**Note**

L'ordre d'exécution des tests est quelconque; il faut donc qu'ils soient indépendants!

---

Méthodes utiles: `Assertions`.

---

**Note**

Ne pas confondre les méthodes de JUnit dont le nom _commence_ par `assert` et les assertion Java!

---

```java
assertEquals(expected, actual);
```
