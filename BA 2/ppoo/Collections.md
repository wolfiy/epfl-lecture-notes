# Collections

On utilise les collections pour manipuler des groupes de valeurs.

## Collections

**Collection:** objet servant de conteneur à d'autres objets (tableaux, listes, ensembles, etc.).

En Java, chaque collection contient généralement une interface qui décrit les opérations offerte par la collection et des classes implémentant celles-ci. On a la hiérarchie (simplifiée) suivante:

<img title="" src="file:///Users/wolfiy/Documents/Screen%20Shot%202022-03-22%20at%2020.12.42.png" alt="" width="287" data-align="center">

---

**Note**

Ne pas confondre `Collection` (l'interface) avec `Collections` (la classe).

---

### Interface "Collection"

Sert d'interface mère aux interfaces `List` et `Set`. On y trouve que des méthodes qui ne dépendent *pas* d'une notion d'ordre.

#### Méthodes principales

| Consultation              | Ajout                | Suppression                  |
| ------------------------- |:--------------------:|:----------------------------:|
| `boolean isEmpty()`       | `b add(E e)`         | `void clear()`               |
| `int size()`              | `b addAll(Col<E> c)` | `b remove(Object e)`         |
| `b contains(Object e)`    |                      | `b removeAll(Col<E> c)`      |
| `b containsAll(Col<E> c)` |                      | `b removeIf(Predicate<E> p)` |
|                           |                      | `b retainAll(Col<E> c)`      |

Note: j'ai abrégé  `Collection` par `Col` et `boolean` par `b`.

#### Méthodes optionnelles

les méthodes de modifications sont optionnelles, car il se peut qu'une collection soit non modifiable.

##### Collection non modifiable

- Utiliser une méthode retournant une collection immuable;

- Obtenir une vue non modifiable avec le préfix `unmodifiable`.

## Listes

Collection d'objet ordonnée. Contrairement aux tableaux, les listes sont de taille variable et l'accès aux données y est séquentiel (et non aléatoire).

### Interface "List"

```java
public interface List<E> extends Collection<E> {
  // … méthodes
}
```

##### Consultation

[`E get(int i)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#get(int)), [`int indexOf(Object e)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#indexOf(java.lang.Object)), [`int lastIndexOf(Object e)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#lastIndexOf(java.lang.Object)).

##### Ajout

[`void add(int i, E e)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#add(int,E)), [`boolean addAll(int i, Collection<E> c)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#addAll(int,java.util.Collection)).

##### Modification

[`E remove(int i)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#remove(int)), [`E set(int i, E e)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#set(int,E)), [`void replaceAll(UnaryOperator<E> op)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#replaceAll(java.util.function.UnaryOperator)).

#### Sous-liste

```java
// return une vue sur la sous-liste composée des éléments compris
// entre les indexs b inclusif et e exclusif.
List<E> subList(int b, int e)
```

#### Tri et mélange

```java
void sort(Comparator<E> c)            // Trie au moyen de c
<T> void shuffle(List<T> l, Random r) // Mélange aléatoirement
```

#### Vues

```java
// return une vue sur un tableau contenant les éléments donnés.
<T> List<T> asList(T... a)
```

La vue retournée est partiellement modifiable: on peut en changer ses éléments à l'aide de `set`, mais on ne peut pas changer sa taille ou son nombre d'éléments.

#### Immuabilité

Pour construire des listes immuables, on a:

```java
<E> List<E> of(E... es)             // Contient les éléments donnés.
<E> List<E> copyOf​(Collection<E> c) // Contient les élé. de la col.
<T> List<T> nCopies(int n, T e)     // Contient n fois e.
```

#### Vues non modifiables

```java
<T> List<T> unmodifiableList(List<T> l)
```

---

**Note**

Pour obtenir une liste immuable à partir d'une liste quelconque, il faut utiliser `copyOf` de `List`.

---

## Parcours des collections

### Itérateurs

Permet d'obtenir l'élément qu'il désigne et sait se déplacer efficacement sur les autres éléments de la collection.

```java
public interface Iterator<E> {
  // … méthodes
} 
```

Trois méthodes: [`boolean hasNext()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Iterator.html#hasNext()), [`E next()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Iterator.html#next()), [`void remove()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Iterator.html#remove()) (optionnelle).

`.get(i)` n'est pas une bonne idée pour les `LinkedLists`, car elle pourrait être $O(n^2)$.

Note: `for-each` est $O(n)$ et donc mieux (Java réécrit une boucle avec `i.next()`.

```java
List<String> l = /* … */;
for (String s: l)
  System.out.println(s);
```

 ---

**Note**

Pour parcourir une liste, utiliser `for-each` sauf s'il faut accéder directement à l'opérateur. *Eviter* d'utiliser `get` sauf pour les tableaux-listes.

---

#### Interface "Iterable"

Seule méthode: `iterator` (abstraite).

#### Itérateurs de listes

Méthode [`listIterator`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html#listIterator()) pour se déplacer, connaitre les index, ajouter et changer les éléments.

## Piles, files, deques

### Pile (stack)

Liste dont les éléments sont toujours insérés ou supprimés à la même extrémité (le "sommet").

### File (queue)

Liste dont les éléments sont toujours insérés à une extrémité et retirés de l'autre.

### Deque (double-ended queue)

Liste dont les éléments sont toujours insérés et supprimés à l'une des deux extrémités.

TODO: schéma

### Interface "Queue"

| Consultation  | Ajout                | Suppression  |
| ------------- | -------------------- | ------------ |
| `E element()` | `boolean add(E e)`   | `E remove()` |
| `E peek()`    | `boolean offer(E e)` | `E poll()`   |

### Interface "Deque"

Généralise `Queue`.

| Equivalent `Queue`                                                                                       | au début                                                                                                         | à la fin                                                                                                       |
| -------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- |
| [`element`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Queue.html#element()) | [`getFirst`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#getFirst())       | [`getLast`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#getLast())       |
| [`peek`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Queue.html#peek())       | [`peekFirst`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#peekFirst())     | [`peekLast`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#peekLast())     |
| [`add`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Queue.html#add(E))        | [`addFirst`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#addFirst(E))      | [`addLast`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#addLast(E))      |
| [`offer`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Queue.html#offer(E))    | [`offerFirst`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#offerFirst(E))  | [`offerLast`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#offerLast(E))  |
| [`remove`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Queue.html#remove())   | [`removeFirst`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#removeFirst()) | [`removeLast`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#removeLast()) |
| [`poll`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Queue.html#poll())       | [`pollFirst`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#pollFirst())     | [`pollLast`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Deque.html#pollLast())     |

---

**Note**

Pour représenter une pile/file/deque, il faut utiliser `ArrayDeque`. Pour représenter une liste dans toute sa généralité, `ArrayList`. Si les opération d'indexation `get` et `set` dominent ou si les ajouts/suppressions se font toujours à la fin, `ArrayList` aussi. Sinon, `LinkedList`.

## Ensembles

Un `set`est une collection non ordonnée d'objets dans laquelle un objet peut apparaitre plusieurs fois.

### Interface "Set"

```java
public interface Set<E> extends Collection<E> { }
```

Permet d'être plus rapide quand, par exemple, on veut comparer les éléments communs de deux listes. On a les opération suivantes:

| Opération    | Méthode       |
|:------------:|:-------------:|
| union        | `addAll`      |
| appartenance | `contains`    |
| inclusion    | `containsAll` |
| différence   | `removeAll`   |
| intersection | `retainAll`   |

A noter que:

- `TreeSet` stocke de manière triée;

- `HashSet` stocke dans un ordre quelconque.

### Ensembles immuables

```java
<E> Set<E> of(E... es)
<E> Set<E> copyOf​(Collection<E> c)
```

### Ensembles non modifiables

```java
<E> Set<E> unmodifiableSet(Set<E> s)
```

---

**Note**

Pour obtenir un ensemble immuable à partir d'un ensemble quelconque, utiliser `Set.copyOf()`.

---

### Parcours des ensembles

L'interface `Iterable` est implémentée dans les ensembles, on peut faire pareil que pour les listes.

---

**Note**

Les éléments d'un ensemble ne sont *pas* ordonnés! `TreeSet` $(O(\log{n}))$ parcours les éléments dans l'ordre croissant, `HashSet` $(O(1))$dans un ordre arbitraire qui peut changer entre les exécutions du programme ou les instances.

En pratique, préférer `HashSet`, sauf pour un ordre croissant.

---

## Tables associatives

Collection qui associe des valeurs (*values*) à des clés (*keys*).

```java
Map<Character, String> morse =
  Map.of('a', ".-",
     'j', ".---",
     'v', "...-");
// … à compléter avec le reste de l'alphabet morse

String java = "java";
for (int i = 0; i < java.length(); ++i)
  System.out.print(morse.get(java.charAt(i)) + " ");
```

### Interface "Map"

```java
/**
 * @param K key
 * @param V value
 */
public interface Map<K, V> {
  // … méthodes
}
```

#### Consultation

[`boolean isEmpty()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#isEmpty()), [`int size()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#size()), [`boolean containsKey(Object k)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#containsKey(java.lang.Object)).

[`V get(Object k)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#get(java.lang.Object)), [`V getOrDefault(Object k, V d)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#getOrDefault(java.lang.Object,V)).

#### Ajout et modification

[`V put(K k, V v)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#put(K,V)), [`V putIfAbsent(K k, V v)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#putIfAbsent(K,V)), [`void putAll(Map<K, V> m)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#putAll(java.util.Map)), [`V computeIfAbsent(K k, Function<K,V> f)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#computeIfAbsent(K,java.util.function.Function)), [`V merge(K k, V v, BiFunction<V,V,V> f)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#merge(K,V,java.util.function.BiFunction)), 

#### Remplacement

[`V replace(K k, V v)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#replace(K,V)), [`boolean replace(K k, V v1, V v2)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#replace(K,V,V)), [`void replaceAll(BiFunction<K,V,V> f)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#replaceAll(java.util.function.BiFunction)).

#### Suppression

[`void clear()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#clear()), [`V remove(Object k)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#remove(java.lang.Object)), [`boolean remove(Object k, Object v)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#remove(java.lang.Object,java.lang.Object)).

#### Vues

[`Set<K> keySet()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#keySet()), [`Collection<V> values()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#values()), [`Set<Map.Entry<K, V>> entrySet()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#entrySet()), [`Set<Map.Entry<K, V>> entrySet()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#entrySet()).

### Interface "Map.Entry"

Deux méthodes: `K getKey()` et `V getValue()`.

### Tables immuables

[`<K, V> Map<K, V> of()`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#of()), [`<K, V> Map<K, V> of(K k, V v)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#of(K,V)), [`<K, V> Map<K, V> of(K k1, V v1, K k2, V v2)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#of(K,V,K,V)), [`<K,V> Map<K,V> copyOf​(Map<K,V> m)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Map.html#copyOf(java.util.Map)).

### Tables non modifiables

[`<K,V> Map<K, V> unmodifiableMap(Map<K, V> m)`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Collections.html#unmodifiableMap(java.util.Map)).

---

**Note**

Pour obtenir une table associative immuable à partir d'une table quelconque, utiliser `Map.copyOf()`. Aussi, les clés sont uniques.

---

### Parcours des tables

/!\ `Map` n'étant *pas* l'interface `Iterable`. A la place, on parcours directement les paires clé/valeurs par l'intermédiaire d'une vue:

```java
Map<String, Integer> m = /* … */;
for (Map.Entry<String, Integer> e: m.entrySet())
  System.out.println(e.getKey() + "->" + e.getValue());
```

Il y a aussi une méthode `forEach` (différente de celle des listes):

```java
Map<String, Integer> m = /* … */;
m.forEach((k, v) -> System.out.println(k + "->" + v));
```

### Mise en oeuvre

- `TreeMap` exige que les clés soient comparables et fourni les opérations en $O(\log{n})$, en parcourant dans l'ordre croissant des clés.

- `HashMap` exige que les clés soient hachables et fourni les opération en $O(1)$, mais ne donne aucune garantie quant à l'ordre de parcours.

A noter que:

- un ensemble peut être vu comme une table associative dont seules les clés importent, les valeurs étant ignorées,
- une table associative peut être vue comme un ensemble de paires clef/valeur — pour peu que la valeur soit ignorée dans les tests d'égalité, les comparaisons et le hachage.

# 
