# Types entiers

En Java, les entiers sont représentés par des vecteurs de bits.

## Représentation des entiers

Les entiers en Java sont donc représentés par des vecteurs de bits de taille fixe.

| Type  | Longueur |
|:-----:|:--------:|
| byte  | 8 bits   |
| short | 8 bits   |
| char  | 8 bits   |
| int   | 32 bits  |
| long  | 64 bits  |

### Interprétation non signée

Le vecteur est interprété comme un nombre exprimé en base 2: 

$n = \displaystyle\sum_{i=0}^{N-1}b_i \cdot 2^i$

Inconvénient: pas de valeurs négatives.

### Complément deux-à-deux

Le bit de poids fort (le plus à gauche) donne le signe: s'il vaut 0, l'entier est positif, s'il vaut 1, le nombre est négatif.

## Notation des entiers

```java
int earthRadius = 6371;
int earthRadius = 6_371;
long earthPopulation = 7_130_000_000L;
int twelve = 0b1100;
long twelveAsLong = 0b1100L;
int minusOne = 0b11111111_11111111_11111111_11111111;
int twelve = 0xC;                       // vaut 12
int thirtyTwo = 0x20;                   // vaut 32
int x = 0xDEAD_BEEF;                    // vaut -559038737
long minusOne = 0xFFFF_FFFF_FFFF_FFFFL; // vaut -1 (long)
int thirty    =  30; // vaut 30
int notThirty = 030; // vaut 24 (!)
```

## Opération arithmétiques

On a les opérations:

- x + y (addition);

- x - y (soustraction);

- x  \* y (multiplication);

- x / y (division entière);

- x % y (reste de la division).

### Overflow

Ces opérations peuvent mener à des dépassement de capacité si le résultat n'est pas représentable (pas assez de bits, MSB qui change de valeur, etc.).

## Opérations bit à bit

```java
int x = 0b0111;    
~x;               // Inversion des bits
x & y;            // Conjonction (et)
x | y;            // Disjonction (ou)
x ^ y;            // Disjonction exclusive (xor)
x << y;           // Décalage arithmétique à gauche (des 0)
x >> y;           // Décalage arithmétique à droite
x <<< y;          // Décalage logique à gauche (des 1)
```

_Remarque:_ un décalage à gauche de $n$ bits est équivalent à une multiplication par $2^n$.

## Dans l'API Java

```java
String toString(int i, int b) // chaine de l'entier en base b
String toString(int i)        // base 10
int parseInt(String s, int b) // autre sens
int parseInt(String s)

int bitCount(int i)
int numberOfLeadingZeros(int i)
int numberOfTrailingZeros(int i)

int lowestOneBit(int i)
int highestOneBit(int i)

int rotateLeft(int i, int d)  // rotation des bits de i de d à gauche
int rotateRight(int i, int d)

int reverse(int i)
int reverseBytes(int i)
```

## Empaquetage

Cf. couleurs QR code 2019.
