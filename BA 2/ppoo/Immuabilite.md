# Immuabilité

Une classe est immuable si l'état de ses instances ne peut pas changer après leur création.

Les copies défensives peuvent poser problème:

- Elles doivent être faites par tous les utilisateurs de la classe non immuable;

- Il est très facile de les oublier, et les problèmes posés par ces oublis sont difficiles à diagnostiquer.

---

**Note**

Autant que possible, définir des classes immuables.

---

## Classes immuables

### Avantages

- Raisonnement sur leur état simple et l'état de leurs instances ne change pas;

- Nul besoin de faire des copies défensives;

- Jamais nécessaire de copier leurs instances;

- Peuvent être partagées entre plusieurs _threads_.

### Désavantages

- Si la classe représente une valeur qui doit souvent changer, créer une nouvelle instance à chaque fois coute en performances;

- Lorsqu'il faut que le changement d'état d'une classe soit visible à tous les possesseurs d'une référence à celui-ci, l'immuabilité rend les choses compliquées.

*Remarque:* l'immuabilité n'est devenue réaliste que récemment; les anciennes bibliothèques de Java contiennent beaucoup de classes non immuables.

### Immuable / non modifiable

Ce n'est pas la même chose! Cependant, une classe immuable est toujours non-modifiable.

**Immuable:** si les instances ne peuvent pas changer d'état une fois créées.

**Non-modifiable:** si un morceau de code ayant accès à l'une des instances de la classe n'a pas la possibilité d'appeler des méthodes modifiant son état. 

### Classes immuables

Une classe est immuable si:

- Ses attributs sont tous finaux, initialisés lors de la construction et jamais modifiés;

- Toute valeur non immuable fournie au constructeur est copiée en profondeur avant d'être stockée dans un de ses attributs;

- Aucune valeur non immuable stockée dans un de ses attributs n'est fournie à l'extérieur;

- Dans la plupart des cas, la classe est elle-même finale.

*Remarque:* les enregistrements sont presque immuables; il reste à s'assurer que leur constructeur copie les éventuelles valeurs non immuables.

## Tableaux et immuabilité

Un tableau "normal" est toujours modifiable en Java. Dans les classes immuables, il faut donc en faire des copies défensives avant de les fournir à l'extérieur.

Pour les tableaux dynamiques, on peut utiliser la méthode `unmodifiableList()`, qui permet d'en obtenir une version non modifiable. Il ne sera alors pas possible de la modifier. Ainsi:

1. Copier défensivement les tableaux reçus à la construction, les rendre non-modifiable et les stocker dans les attributs;

2. Retourner ces tableaux non modifiables par des méthodes d'accès.

Exemple d'un vecteur immuable:

```java
public final class Vector {
  private final List<Double> v;
  public Vector(List<Double> a) {
    v = unmodifiableList(new ArrayList<Double>(a));
    v = List.copyOf(a) // Aussi possible (Java 10+).
  }
  public List<Double> asList() {
    return v;
  }
}
```

## Bâtisseurs

Utilisés pour facilité la construction par étapes d'instances de classes immuables. Ils permettent la construction progressive d'instances.

Comme ils ne sont que brièvement utilisés pour construire des instances et _non_ pour les représenter, ils ne posent pas les même problèmes que version non immuable.

Un bâtisseur n'est utilisé que très localement et n'est _pas_ stocké dans un attribut.

### Bâtisseurs imbriqués

Pour imbriquer un bâtisseur il faut:

- Déplacer la classe `XyzBuilder` dans la classe `Xyz`;

- Lui attacher l'attribut `static` pour en faire une _classe imbriquée statiquement_;

- La renommer en `Builder`.

#### Classe statiquement imbriquée

Similaire à une classe non imbriquée. Différences:

1. De l'extérieur, le nom de la classe imbriquée est précédé de celui de la classe englobante (p.ex. `Date.Builder`);

2. Une classe statiquement imbriquée a accès aux membres privés `static` de la classe englobante;

3. Une classe imbriquée statiquement peut être déclarée privée ou protégée.

*Remarque:* elle ne peut accéder qu'aux membres statiques de la classe englobante.

---

**Note**

S'il peut être utile de construire par étapes des instances d'une classe immuable, lui associer un bâtisseur. De plus:

- Nommer la classe `Builder`;

- L'imbriquer statiquement;

- Nommer sa méthode de construction `build`;

- Retourner `this` de toutes les méthodes de modification pour permettre les appels chainés.

---


