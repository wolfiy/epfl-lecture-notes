# Notes de cours EPFL
Section IN, 2020-????

## Fichiers PDF
Les liens direct de téléchargement/ouverture sont ci-dessous.

### BA2
[Analyse II](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%202/analyse-II/analyse-II.pdf)

[AICC II](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%202/aicc-II/aicc-II.pdf)

[DSD](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%202/dsd/dsd.pdf)

[PPOO](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%202/ppoo/ppoo.pdf)

[Energie B](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%202/shs/shs.pdf)

### BA1
[Analyse I](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%201/analyse-I/analyse-I.pdf)

[Algèbre linéaire](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%201/algebre-lineaire/algebre-lineaire.pdf)

[Physique mécanique](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%201/physique/physique.pdf)

[AICC I](https://gitlab.com/wolfiy/epfl-lecture-notes/-/raw/master/BA%201/aicc-I/aicc-I.pdf)

## C'est quoi?
Dans ce _repository_, vous trouverez certaines des notes que j'aurai prises durant mes études à l'EPFL. Il s'agira principalement de résumés, mais il se peut que j'y ajoute mes notes manuscrite plus tard.

Dans ces résumés, vous trouverez en majeur partie des théorèmes, propositions, définitions et propriétés, mais j'y ajouterai aussi les _tricks_ qui ne nous sont pas forcément  enseignés par les professeurs (merci aux assistants!). J'ai aussi fait la MàN en 2021, donc j'y glisserai aussi certaines choses que j'y ai apprise qui me semblent utiles de connaitre pour la suite (d'autres _tricks_, théorèmes et propriétés considérés comme acquis, etc.). Sur le long terme, l'idée est d'y rassambler mes notes de chaques cours, et de chaques semestres.

Ayant déjà validé le cours d'introduction à la programmation je ne le résumerai pas.

Les sources (quel cours, quel prof, ...) sont indiquées au début de chaque document. Les éventuelles sources secondaires utilisées sont en note de bas de page.

Aussi, je débute avec Latex, donc le code risque d'être un peu moche au début, mais je corrigerai ça au fur et à mesure que je m'améliore (si vous avez des conseils, hésitez pas!).

__Je ne garanti pas l'absence d'erreurs ou de typos, si vous en trouvez, n'hésitez pas à m'en faire part__ (Discord: `wolfiy.#0405`).

## Autres
Si vous vous demandiez, j'utilise [VSCodium](https://vscodium.com/) avec l'extension _LaTeX Workshop_ (v. 8.19.2, il y a des problèmes de compatibilité avec versions plus récentes et VSCodium).